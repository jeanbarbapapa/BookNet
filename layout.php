<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/flatly.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body>
    <nav class="aside-navbar">
        <div class="profile">
            <img src="image/avatar/default_profile.png" class="img-circle">
            <p>
                <b>Jean-Baptiste Caplan</b>
            </p>
        </div>
        <div class="items">
            <ul>
                <li>
                    <a><i class="fa fa-book"></i>Accueil</a>
                </li>
                <li>
                    <a><i class="fa fa-bullhorn"></i>Nouveautés</a>
                </li>
                <li class="active">
                    <a><i class="fa fa-bookmark-o"></i>Livres</a>
                </li>
            </ul>
        </div>
    </nav>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>