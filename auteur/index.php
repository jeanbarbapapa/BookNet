<?php
session_start();
// On vérifie que l'utilisateur est connecté
if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
    $user = $_SESSION['user'];
} else {
    // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
    header('Location: ../login.php');
    // TODO: Page vitrine et redirection
    exit('Redirection... <a href="">Cliquez ici</a>');
}
// On définit la route actuelle pour l'affichage dans la navigation
$route = 'auteur';

// Tentative connexion à la base de données
try {
    $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
} catch (Exception $e) {
    // En cas d'erreur on quitte proprement en affichant un message controllé
    die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
}

define("NOMBRE_AUTEUR_PAR_PAGE", 16);

// On détermines le nombre de page dont on a besoin
$query = $db->query('SELECT COUNT(*) AS nombre_auteur FROM auteur');
$informations = $query->fetch();
$informations['nombre_auteur'] = intval($informations['nombre_auteur']);

$reste = $informations['nombre_auteur'] % NOMBRE_AUTEUR_PAR_PAGE;
$quotient = ($informations['nombre_auteur'] - $reste) / NOMBRE_AUTEUR_PAR_PAGE;
$nombrePage = $quotient;
// Si quelques page sont en plus on ajoute une page de plus
if ($reste > 0) {
    $nombrePage++;
}

// On récupère la page demandé, si aucune valeur n'est fournit ou est < 1 ou est > au nombre de page on donne la valeur 1
$pageRequested = (isset($_GET['page']) && intval($_GET['page']) >= 1 && intval($_GET['page']) <= $nombrePage) ? intval($_GET['page']) : 1;

// On détermine à partir de la page demandé l'offset et la limite pour la requête de sélectionne d'auteur
// L'offset correspond au rang à partir du quel on récupère
// La limite correspond au nombre de rang que l'on récupère à partir de l'offset
$offset = ($pageRequested - 1) * NOMBRE_AUTEUR_PAR_PAGE;
$limite = $offset + NOMBRE_AUTEUR_PAR_PAGE;

// On récupère les auteurs
// préparation de la requête
$query = $db->query("SELECT
                      id,
                      IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), auteur.pseudo) AS auteur_shortname,
                      -- Si la date de naissance est NULL on recupère un ?
                      IFNULL(naissance, \"?\") AS naissance,
                      -- Si la date de decès est NULL on recupère un ?
                      IFNULL(mort, \"?\") AS mort
                      FROM auteur
                      ORDER BY auteur.nom ASC
                      -- On applique l'offset et la limite calculée plus haut
                      LIMIT $offset, $limite");
$auteurs = $query->fetchAll();




?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Liste des auteurs</title>

    <!-- Bootstrap -->
    <link href="../css/flatly.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/fonts.css" rel="stylesheet">
    <link href="../css/chosen.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

</head>
<body>
<?php include('../navigation.php') ?>

    <div class="page-wrapper">
        <div class="rowbox">
            <h2>Liste des auteurs</h2>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <a href="ajouter.php" class="btn btn-success btn-outline btn-block btn-sm">Ajouter un auteur</a>
                </div>
            </div>
        </div>
        <div class="rowbox">
            <div>
                <ul class="list-inline list-item">
                <?php foreach($auteurs as $auteur): ?>
                    <li class="text-center">
                        <a href="fiche.php?auteur_id=<?php echo $auteur['id'] ?>">
                            <i class="fa fa-user fa-lg"></i><br>
                            <?php echo $auteur['auteur_shortname'] ?><br>
                            <small><?php echo $auteur['naissance'].' - '.$auteur['mort'] ?></small>
                        </a>
                    </li>
                <?php endforeach ?>
                </ul>
            </div>
            <ul class="pagination pagination-sm">
                <?php if ($nombrePage > 1): ?>
                    <?php foreach (range(1, $nombrePage) as $pageId => $pageNumber): ?>
                        <li <?php if ($pageNumber == $pageRequested):?>class="active"<?php endif ?>><a href="?page=<?php echo $pageNumber ?>"><?php echo $pageNumber ?></a></li>
                    <?php endforeach ?>
                <?php endif ?>
            </ul>
        </div>
        <footer>
            <div class="text-center">
                <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
            </div>
        </footer>
    </div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.min.js"></script>
<!-- Chosen -->
<script src="../js/chosen.jquery.min.js"></script>

<script>
    $('.chosen-select').chosen();
</script>
</body>
</html>