<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'livre';

    $GLOBALS['erreurs'] = array();

    $GLOBALS['dictionnaire'] = array(
        'livre_auteur_id' => array('entity' => 'auteur', 'label' => 'auteur', 'type' => 'entity'),
        'livre_titre' => array('maxlength' => 150, 'label' => 'titre', 'type' => 'string'),
        'livre_date' => array('min' => -3000, 'max' => 2016, 'label' => 'date de parution', 'type' => 'integer'),
        'livre_resume' => array('maxlength' => 3000, 'label' => 'résumé', 'type' => 'string'),
        'livre_categorie_id' => array('entity' => 'categorie', 'label' => 'categorie', 'type' => 'entity')
    );

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // Récupération de la liste des auteurs
    $query = $db->query('SELECT
                          id AS auteur_id,
                          IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, \' \', auteur.nom), auteur.pseudo) AS auteur_shortname
                          FROM auteur');
    $auteurs = $query->fetchAll();

    $query = $db->query('SELECT * FROM categorie ORDER BY nom');
    $categories = $query->fetchAll();

    // valeurs par défaut
    $livre_auteur_id = 0;
    $livre_titre = '';
    $livre_date = 2000;
    $livre_resume = '';
    $livre_categorie_id = 0;

    // Si une valeur est fournit on la récupère
    if (isset($_GET['auteur_id'])) {
        $livre_auteur_id = intval($_GET['auteur_id']);
    }

    if (isset($_POST['livre_titre'])) {

        // On récupère chaque valeur à l'aide de notre fonction retrieve_input() qui efectue tous les tests pour
        // singalé une potentielle erreur par rapport aux informations indiquées dans le dictionnaire global
        $livre_auteur_id = retrieve_input('livre_auteur_id');
        $livre_titre = retrieve_input('livre_titre');
        $livre_date = retrieve_input('livre_date');
        $livre_resume = retrieve_input('livre_resume');
        $livre_categorie_id = retrieve_input('livre_categorie_id');

        if (count($GLOBALS['erreurs']) == 0) {

            // On vérifie si l'association titre/auteur/date existe déjà
            // préparation de la requête
            $query = $db->prepare('SELECT id FROM livre WHERE titre = ? AND date = ? AND auteur_id = ?');
            $query->execute(array(
                $livre_titre,
                $livre_date,
                $livre_auteur_id
            ));
            if ($query->rowCount() > 0) {
                $GLOBALS['erreurs'][] = "Le livre \"$livre_titre\" par cet auteur existe déjà";
            }

            // Valeurs par défaut
            $livre_couverture = '';

            if (count($GLOBALS['erreurs']) == 0) {
                $query = $db->prepare('INSERT INTO livre VALUES(NULL, :titre, :auteur_id, :utilisateur_id, :resume, :date, :couverture, :categorie_id)');
                $query->execute(array(
                    ':titre' => $livre_titre,
                    ':auteur_id' => $livre_auteur_id,
                    ':utilisateur_id' => $user['id'],
                    ':resume' => $livre_resume,
                    ':date' => $livre_date,
                    ':couverture' => $livre_couverture,
                    ':categorie_id' => $livre_categorie_id
                ));

                // On récupère l'id du livre que l'on vient d'ajouter
                $query = $db->query('SELECT id FROM livre ORDER BY id DESC LIMIT 0, 1');
                $livre_id = $query->fetch()['id'];

                // On redirige vers la fiche du livre
                header('Location: fiche.php?livre_id='.$livre_id);
                exit('Redirection... <a href="fiche.php?livre_id='.$livre_id.'">Cliquez ici</a>');
            }

        }

    }

    function retrieve_input($input_name) {
        // On récupère la valeur depuis $_POST
        $value = $_POST[$input_name];
        $informations = $GLOBALS['dictionnaire'][$input_name];
        if ($informations['type'] == 'string') {
            // On convertit value en string
            $value = strval($value);
            // Si la valeur est un "?" on renvoit null, c'est à dire rien
            if ($value == '?') {
                return null;
            }
            // On récupère la longeur minimum ou 1 si elle n'est pas définit
            $minlength = (isset($informations['minlength'])) ? $informations['minlength'] : 1;
            // On récupère la longeur maximum ou 2 si elle n'est pas définit
            $maxlength = (isset($informations['maxlength'])) ? $informations['maxlength'] : 4;
            // Si la longueur de la chaine dépasse la longueur maximum ou est inférieur à la longueur minimum on ajoute un erreur
            if (strlen($value) < $minlength || strlen($value) > $maxlength) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La longueur de $label n'est pas respectée (entre $minlength et $maxlength caractères)";
            }
            // Si aucune valeur n'est spécifiée on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }

            // On sécurise la chaine de caractère en échappant tous les caractères spécifique à l'HTML
            $value = htmlspecialchars($value);
        } elseif ($informations['type'] == 'integer') {
            // Si aucune valeur n'est précisé on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }
            // Si la valeur est un "?" on renvoit null, c'est à dire rien
            if ($value == '?') {
                return null;
            }
            // On convertit value en un entier
            $value = intval($value);
            // Si la valeur spécifiée dépasse les borne défini on ajoute une erreur
            if ($value < $informations['min'] || $value > $informations['max']) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
            }
        } elseif ($informations['type'] == 'entity') {
            $value = intval($value);
            // Tentative connexion à la base de données
            try {
                $db = new PDO('mysql:host=jeanbafrix-1.mysql.db;dbname=jeanbafrix-1', 'jeanbafrix-1', 'MonPanda45', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            } catch (Exception $e) {
                // En cas d'erreur on quitte proprement en affichant un message controllé
                die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
            }
            // On prépare un requête pour vérifier si l'entité existe bel et bien
            $query = $db->prepare('SELECT * FROM '.$informations['entity'].' WHERE id = ?');
            $query->execute(array(
                $value
            ));
            if ($query->rowCount() != 1) {
                // Si elle n'existe pas on ajoute une erreur
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
            }
        }

        // On retourne la valeur
        return $value;
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Ajouter un livre</title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/chosen.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">

            <div class="rowbox">
                <h2>Ajouter un livre</h2>

                <hr>

                <form method="post" class="container">

                    <?php if (count($GLOBALS['erreurs']) > 0): ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <? foreach($GLOBALS['erreurs'] as $erreur): ?>
                                    <li><?php echo $erreur ?></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    <?php endif ?>

                    <div class="form-group">
                        <label>Sélectionner votre auteur</label>
                        <select name="livre_auteur_id" class="form-control chosen-select">
                            <?php foreach($auteurs as $auteur): ?>
                            <option value="<?php echo $auteur['auteur_id'] ?>" <?php if($auteur['auteur_id'] == $livre_auteur_id): ?>selected<?php endif ?>><?php echo $auteur['auteur_shortname'] ?></option>
                            <?php endforeach ?>
                        </select>
                        <span class="help-block">Vous ne trouvez pas votre auteur ? <a href="../auteur/ajouter.php">Ajouter le !</a></span>
                    </div>
                    <div class="form-group">
                        <label>Titre</label>
                        <input type="text" class="form-control" name="livre_titre" placeholder="Titre" maxlength="150" value="<?php echo $livre_titre ?>">
                    </div>
                    <div class="form-group">
                        <label>Année de parution</label>
                        <input type="number" class="form-control" name="livre_date" maxlength="4" placeholder="Année de parution" value="<?php echo $livre_date ?>">
                    </div>
                    <div class="form-group">
                        <label>Résumé</label>
                        <textarea class="form-control" name="livre_resume" rows="7" placeholder="Court résumé"><?php echo $livre_resume ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Catégorie</label>
                        <select class="form-control chosen-select" name="livre_categorie_id">
                            <?php foreach($categories as $categorie): ?>
                                <option value="<?php echo $categorie['id'] ?>" <?php if($categorie['id'] == $livre_categorie_id): ?>selected<?php endif ?>><?php echo $categorie['nom'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-primary btn-outline btn-block" href="index.php"><i class="fa fa-list fa-fw"></i> Liste des livres</a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success btn-block">Ajouter</button>
                        </div>
                    </div>
                </form>
            </div>
            <footer>
                <div class="text-center">
                    <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
                </div>
            </footer>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Chosen -->
        <script src="../js/chosen.jquery.min.js"></script>

        <script>
            $('.chosen-select').chosen();
        </script>
    </body>
</html>