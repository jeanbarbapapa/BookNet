<?php
    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'livre';

    $GLOBALS['erreurs'] = array();

    if (isset($_GET['livre_id'])) {
        // Tentative connexion à la base de données
        try {
            $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        } catch (Exception $e) {
            // En cas d'erreur on quitte proprement en affichant un message controllé
            die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
        }

        // On vérifie si l'auteur existe déjà
        // préparation de la requête
        $smallTag = ['<small>', '</small>'];
        $utilisateur_id = $user['id'];
        $query = $db->prepare("SELECT
                               livre.id AS livre_id,
                               livre.titre AS titre,
                               IFNULL(livre.date, '?') AS date,
                               livre.resume AS resume,
                               auteur.id AS auteur_id,
                               -- Si auteur.pseudo est NULL on récupère une string concaténant prenom et nom
                               -- Sinon on recupère une string concatéant le pseudo avec le prenom et le nom en petit
                               IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), auteur.pseudo) AS auteur_shortname,
                               categorie.nom AS categorie,
                               utilisateur.id AS utilisateur_id,
                                CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname,
                               -- On utilise des sous-requete pour récupérer le nombre de favoris sur ce livre
                               (SELECT COUNT(*) FROM utilisateur_livrespreferes WHERE utilisateur_livrespreferes.livre_id = livre.id) AS count_livreprefere,
                               -- On utilise des sous-requete pour récupérer le fait que l'utilisateur à ajouter le livre dans ses favoris
                               EXISTS (SELECT * FROM utilisateur_livrespreferes WHERE utilisateur_livrespreferes.livre_id = livre.id AND utilisateur_livrespreferes.utilisateur_id = $utilisateur_id) AS utilisateur_livreprefere
                               FROM livre
                               -- jointure avec la table auteur pour récupérer les informations sur l'auteur du livre
                               LEFT JOIN auteur ON auteur.id = livre.auteur_id
                               -- jointure avec la table categorie pour récupérer les informations sur la catégorie du livre
                               LEFT JOIN categorie ON categorie.id = livre.categorie_id
                               -- jointure avec la table utilisateur pour récupérer les informations sur l'utilisateur qui a modifé le livre en dernier
                               LEFT JOIN utilisateur ON utilisateur.id = auteur.utilisateur_id
                               WHERE
                               livre.id = :livre_id");
        // On execute la requête en passant en argument l'id du livre demandé
        $query->execute(array(
            ':livre_id' => $_GET['livre_id']
        ));
        if ($query->rowCount() == 1) {
            // Si il y'a bien un livre on stocke ses infos dans une variable
            $livre = $query->fetch();
            // Le livre existe, on va récupérer les critiques qui lui sont associés
            $query = $db->prepare("SELECT
                                   article.id,
                                   IF(article.titre = '', 'SANS TITRE', article.titre) AS critique_titre,
                                   -- article.date
                                   -- On utilise des sous-requete pour récupérer le nombre de likes et commentaires
                                   -- On utilise les ` pour échapper LIKE qui est un mot clé SQL
                                   (SELECT COUNT(*) FROM `like` WHERE `like`.article_id = article.id) AS count_like,
                                   (SELECT COUNT(*) FROM commentaire WHERE commentaire.article_id = article.id) AS count_commentaire,
                                   -- On utilise des sous-requete pour récupérer le fait que l'utilisateur soit abonné
                                   EXISTS (SELECT * FROM abonnements WHERE abonnements.utilisateur_suivi_id = utilisateur.id AND abonnements.utilisateur_abonne_id = $utilisateur_id) AS utilisateur_abonne,
                                   CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname
                                   FROM article
                                   LEFT JOIN utilisateur ON utilisateur.id = article.utilisateur_id
                                   WHERE livre_id = ?
                                   GROUP BY article.id
                                   ORDER BY date_creation DESC -- On récupère par ordre chronologique inversé");
            // On execute la requête en passant en argument l'id du livre demandé
            $query->execute(array($_GET['livre_id']));
            // On stockes les critiques sous l'index "critiques" de la variable dictionnaire "livre"
            $livre['critiques'] = $query->fetchAll();

        } else {
            // Sinon on place la valeur de la variable sur null, c'est à dire rien
            $livre = null;
            // On ajoute une erreur pour l'afficher
            $GLOBALS['erreurs'][] = "Le livre demandé n'existe pas...";
        }

        # On vérfie si on a transmis un auteur via "from"
        if (isset($_GET['from'])) {
            $auteur_id = $_GET['from'];

            // préparation de la requête pour récupérer le auteur
            $query = $db->prepare('SELECT
                                    auteur.id as auteur_id,
                                    IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, \' \', auteur.nom), auteur.pseudo) AS auteur_shortname
                                  FROM auteur
                                  WHERE
                                  auteur.id = ?');
            $query->execute(array($auteur_id));
            if ($query->rowCount() == 1) {
                // Si il y'a bien un auteur on récupère ses infos dans une variable
                $auteur_from = $query->fetch();
            } else {
                // Sinon on place la valeur de la variable sur null, c'est à dire rien
                $auteur_from = null;
            }
        }


    } else {
        // Si aucun auteur n'est fournit on redirige vers la page index.php du dossier livre
        header('Location: index.php');
        // Si pour un raison X ou Y la redirection n'a pas lieu on quitte la page dans tout les cas avec un lien vers
        // la page voulue
        exit('Redirection... <a href="index.php">Cliquer ici</a>');
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Bootstrap 101 Template</title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/chosen.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">


                <?php if (count($GLOBALS['erreurs']) > 0): ?>
                    <div class="rowbox">
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <? foreach($GLOBALS['erreurs'] as $erreur): ?>
                                    <li><?php echo $erreur ?></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                <?php endif ?>

                <?php if (isset($livre) && $livre): ?>
                <div class="rowbox">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-1 col-sm-2 col-sm-offset-1">
                            <p style="padding-top: 35px" class="text-center"><i class="fa fa-book fa-5x"></i></p>
                        </div>
                        <div class="col-sm-9">
                            <h2><b><i><?php echo $livre['titre'] ?></i></b> <?php echo '('.$livre['date'].')' ?> <small class="text-warning"><i class="fa fa-star"></i> <?php echo $livre['count_livreprefere'] ?></small></h2>
                            <h4><a href="../auteur/fiche.php?auteur_id=<?php echo $livre['auteur_id'] ?>&from=<?php echo $livre['livre_id'] ?>"><?php echo $livre['auteur_shortname'] ?></a></h4>
                            <h4><span class="label label-success"><?php echo $livre['categorie'] ?></span></h4>
                        </div>

                        <div class="col-md-10 col-md-offset-1">
                            <p class="text-right small no-margin">Dernières modifications par : <a href="../profil/index.php?utilisateur_id=<?php echo $livre['utilisateur_id'] ?>"><?php echo $livre['utilisateur_fullname'] ?></a></p>

                            <div class="well well-lg">
                                <p><?php echo nl2br($livre['resume']) ?></p>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <a class="btn btn-success btn-outline btn-block btn-sm" href="../critique/ajouter.php?livre_id=<?php echo $livre['livre_id'] ?>"><i class="fa fa-paragraph fa-fw"></i> Rédiger une critique</a>
                                </div>
                                <div class="col-sm-4">
                                    <a href="favorite.php?livre_id=<?php echo $livre['livre_id'] ?>" class="btn btn-success btn-outline btn-block btn-sm">
                                        <?php if ($livre['utilisateur_livreprefere'] > 0): ?>
                                            <i class="fa fa-star fa-fw"></i> Retirer de mes favoris
                                        <?php else: ?>
                                            <i class="fa fa-star-o fa-fw"></i> Ajouter à mes favoris
                                        <?php endif ?>
                                    </a>
                                </div>
                                <div class="col-sm-4">
                                    <a href="editer.php?livre_id=<?php echo $livre['livre_id'] ?>" class="btn btn-success btn-outline btn-block btn-sm"><i class="fa fa-pencil fa-fw"></i> Editer la fiche</a>
                                </div>
                            </div>
                            <div style="margin-top: 15px" class="row">
                                <div class="col-sm-12">
                                    <?php if (isset($auteur_from) && $auteur_from): ?>
                                        <a class="btn btn-primary btn-outline btn-block btn-sm" href="../auteur/fiche.php?auteur_id=<?php echo $auteur_from['id'] ?>"><i class="fa fa-user fa-fw"></i> <?php echo $auteur_from['auteur_shortname'] ?></a>
                                    <?php else: ?>
                                        <a class="btn btn-primary btn-outline btn-block btn-sm" href="index.php"><i class="fa fa-list fa-fw"></i> Liste des livres</a>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="rowbox">
                    <div class="container-fluid">

                        <h3 class="text-uppercase">Critiques</h3>

                        <?php if (count($livre['critiques']) == 0): ?>
                            <p class="text-center">Aucune critique n'a encore été écrite sur <i><?php echo $livre['titre'] ?></i>, soyez le premier à en <a href="../critique/ajouter.php?livre_id=<?php echo $livre['livre_id'] ?>">rédiger une</a> !</p>
                        <?php else: ?>

                            <p>Retrouvez ici toutes les critiques rédigées par nos membres sur <i><?php echo $livre['titre'] ?></i> en commençant par les plus récentes :</p>
                            <div class="critiques-container">
                                <?php foreach($livre['critiques'] as $i => $critique): ?>
                                    <div class="row critique">
                                        <div class="col-sm-6">
                                            <h4 class="text-uppercase text-right">
                                                <a target="_self" href="../critique/lire.php?critique_id=<?php echo $critique['id'] ?>&from=<?php echo $livre['livre_id'] ?>"><?php echo $critique['critique_titre'] ?></a>
                                                <small class="text-danger"><i class="fa fa-heart fa-fw"></i> <?php echo $critique['count_like'] ?></small>
                                                <small class="text-success"><i class="fa fa-comment fa-fw"></i> <?php echo $critique['count_commentaire'] ?></small>
                                            </h4>
                                            <h4 class="text-right">
                                                <small><?php echo $critique['utilisateur_fullname'] ?> <?php if($critique['utilisateur_abonne']): ?><span class="text-grey"><i class="fa fa-rss-square fa-fw"></i></span><?php endif ?></small>
                                            </h4>
                                        </div>
                                        <div class="col-sm-12">
                                            <hr>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        <?php endif ?>

                    </div>
                </div>

                <?php endif ?>
                <footer>
                    <div class="text-center">
                        <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
                    </div>
                </footer>
            </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Chosen -->
        <script src="../js/chosen.jquery.min.js"></script>

        <script>
            $('.chosen-select').chosen();
        </script>
    </body>
</html>