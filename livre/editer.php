<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'livre';

    // On créer un tableau global pour stocker les potentielles erreurs
    $GLOBALS['erreurs'] = array();

    // On créer un dictionnaire global pour enregistrer les informations des différents champs du formulaire
    $GLOBALS['dictionnaire'] = array(
        'livre_nom' => array('maxlength' => 30, 'label' => 'nom livre', 'type' => 'string'),
        'livre_prenom' => array('maxlength' => 30, 'label' => 'prénom livre', 'type' => 'string'),
        'livre_pseudo' => array('maxlength' => 30, 'label' => 'pseudonyme livre', 'type' => 'string'),
        'livre_naissance' => array('min' => -3000, 'max' => 2016, 'label' => 'année de naissance', 'type' => 'integer'),
        'livre_mort' => array('min' => -3000, 'max' => 2016, 'label' => 'année de décès', 'type' => 'integer'),
        'livre_biographie' => array('maxlength' => 3000, 'label' => 'biographie', 'type' => 'string')
    );

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // On récupère l'id de l'livre donné si il est fournit sinon on met en valeur par défaut -1 c'est à dire aucun livre
    $livre_id = (isset($_GET['livre_id'])) ? $_GET['livre_id'] : -1;

    $query = $db->prepare("SELECT
                          livre.id AS livre_id,
                          livre.auteur_id AS auteur_id,
                          livre.titre,
                          livre.date,
                          livre.resume,
                          livre.categorie_id
                          FROM livre
                          WHERE livre.id = ?");
    // On execute la requête en passant en argument l'id de l'livre voulu
    $query->execute(array($livre_id));

    // Aucun livre ne correpond aux critères
    if ($query->rowCount() == 0) {
        header('Location: index.php');
        exit('Redirection... <a href="index.php">Cliquez ici</a>');
    }

    $livre = $query->fetch();

    // On initialise avec des valeurs du livre
    $livre_auteur_id = $livre['auteur_id'];
    $livre_titre = $livre['titre'];
    $livre_date = $livre['date'];
    $livre_resume = $livre['resume'];
    $livre_categorie_id = $livre['categorie_id'];

    // Récupération de la liste des auteurs
    $query = $db->query('SELECT * FROM auteur');
    $auteurs = $query->fetchAll();

    // Récupération de la liste des catégories
$query = $db->query('SELECT * FROM categorie ORDER BY nom');
    $categories = $query->fetchAll();

    // Si on détecte une valeur dans $_POST, c'est ce que le formulaire à été soumis
    if (isset($_POST['livre_titre'])) {

        // On récupère chaque valeur à l'aide de notre fonction retrieve_input() qui efectue tous les tests pour
        // singalé une potentielle erreur par rapport aux informations indiquées dans le dictionnaire global
        $livre_auteur_id = retrieve_input('livre_auteur_id');
        $livre_titre = retrieve_input('livre_titre');
        $livre_date = retrieve_input('livre_date');
        $livre_resume = retrieve_input('livre_resume');
        $livre_categorie_id = retrieve_input('livre_categorie_id');

        // Si il n'y a pas d'erreur
        if (count($GLOBALS['erreurs']) == 0) {

            // On vérifie si l'livre existe
            // préparation de la requête
            $query = $db->prepare('SELECT id
                                  FROM livre
                                  WHERE
                                  id = ?');
            $query->execute(array(
                $livre_id
            ));
            if ($query->rowCount() != 1) {
                // On ajoute une erreur
                $GLOBALS['erreurs'][] = "Le livre \"$livre_prenom $livre_nom\" n'existe pas";
            }

            // Si il n'y a pas d'erreur on procède à la suite
            if (count($GLOBALS['erreurs']) == 0) {

                // On prépare la requête de mise à jour
                $query = $db->prepare('UPDATE livre SET
                                        titre = ?,
                                        auteur_id = ?,
                                        resume = ?,
                                        date = ?,
                                        categorie_id = ?,
                                        utilisateur_id = ?
                                        WHERE
                                        id = ?');
                // On l'execute en passant les valeurs
                $query->execute(array(
                    $livre_titre,
                    $livre_auteur_id,
                    $livre_resume,
                    $livre_date,
                    $livre_categorie_id,
                    $user['id'],
                    $livre_id
                ));

                // On redirige vers la fiche de l'livre
                header('Location: fiche.php?livre_id='.$livre_id);
                exit('Redirection... <a href="fiche.php?livre_id='.$livre_id.'">Cliquez ici</a>');
            }
        }
    }

    function retrieve_input($input_name) {
        // On récupère la valeur depuis $_POST
        $value = $_POST[$input_name];
        $informations = $GLOBALS['dictionnaire'][$input_name];
        if ($informations['type'] == 'string') {
            // On convertit value en string
            $value = strval($value);
            // Si la valeur est un "?" on renvoit null, c'est à dire rien
            if ($value == '?') {
                return null;
            }
            // On récupère la longeur minimum ou 1 si elle n'est pas définit
            $minlength = (isset($informations['minlength'])) ? $informations['minlength'] : 1;
            // On récupère la longeur maximum ou 2 si elle n'est pas définit
            $maxlength = (isset($informations['maxlength'])) ? $informations['maxlength'] : 4;
            // Si la longueur de la chaine dépasse la longueur maximum ou est inférieur à la longueur minimum on ajoute un erreur
            if (strlen($value) < $minlength || strlen($value) > $maxlength) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La longueur de $label n'est pas respectée (entre $minlength et $maxlength caractères)";
            }
            // Si aucune valeur n'est spécifiée on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }

            // On sécurise la chaine de caractère en échappant tous les caractères spécifique à l'HTML
            $value = htmlspecialchars($value);
        } elseif ($informations['type'] == 'integer') {
            // Si aucune valeur n'est précisé on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }
            // Si la valeur est un "?" on renvoit null, c'est à dire rien
            if ($value == '?') {
                return null;
            }
            // On convertit value en un entier
            $value = intval($value);
            // Si la valeur spécifiée dépasse les borne défini on ajoute une erreur
            if ($value < $informations['min'] || $value > $informations['max']) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
            }
        } elseif ($informations['type'] == 'entity') {
            $value = intval($value);
            // Tentative connexion à la base de données
            try {
                $db = new PDO('mysql:host=jeanbafrix-1.mysql.db;dbname=jeanbafrix-1', 'jeanbafrix-1', 'MonPanda45', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            } catch (Exception $e) {
                // En cas d'erreur on quitte proprement en affichant un message controllé
                die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
            }
            // On prépare un requête pour vérifier si l'entité existe bel et bien
            $query = $db->prepare('SELECT * FROM '.$informations['entity'].' WHERE id = ?');
            $query->execute(array(
                $value
            ));
            if ($query->rowCount() != 1) {
                // Si elle n'existe pas on ajoute une erreur
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
            }
        }

        // On retourne la valeur
        return $value;
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Editer: <?php echo $livre['titre'] ?></title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/chosen.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">

            <div class="rowbox">
                <h2>Edition de <i><?php echo $livre['titre'] ?></i></h2>

                <hr>

                <form method="post" class="container">

                    <?php if (count($GLOBALS['erreurs']) > 0): ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <? foreach($GLOBALS['erreurs'] as $erreur): ?>
                                    <li><?php echo $erreur ?></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    <?php endif ?>

                    <div class="form-group">
                        <label>Sélectionner votre auteur</label>
                        <select name="livre_auteur_id" class="form-control chosen-select">
                            <?php foreach($auteurs as $auteur): ?>
                                <option value="<?php echo $auteur['id'] ?>" <?php if($auteur['id'] == $livre_auteur_id): ?>selected<?php endif ?>><?php echo $auteur['prenom'].' '.$auteur['nom'] ?></option>
                            <?php endforeach ?>
                        </select>
                        <span class="help-block">Vous ne trouvez pas votre auteur ? <a href="../auteur/ajouter.php">Ajouter le !</a></span>
                    </div>
                    <div class="form-group">
                        <label>Titre</label>
                        <input type="text" class="form-control" name="livre_titre" placeholder="Titre" maxlength="150" value="<?php echo $livre_titre ?>">
                    </div>
                    <div class="form-group">
                        <label>Année de parution</label>
                        <input type="number" class="form-control" name="livre_date" maxlength="4" placeholder="Année de parution" value="<?php echo $livre_date ?>">
                    </div>
                    <div class="form-group">
                        <label>Résumé</label>
                        <textarea class="form-control" name="livre_resume" rows="7" placeholder="Court résumé"><?php echo $livre_resume ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Catégorie</label>
                        <select class="form-control chosen-select" name="livre_categorie_id">
                            <?php foreach($categories as $categorie): ?>
                                <option value="<?php echo $categorie['id'] ?>" <?php if($categorie['id'] == $livre_categorie_id): ?>selected<?php endif ?>><?php echo $categorie['nom'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-primary btn-outline btn-block" href="fiche.php?livre_id=<?php echo $livre['livre_id'] ?>"><i class="fa fa-book fa-fw"></i> <?php echo $livre['titre'] ?></a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success btn-block">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
            <footer>
                <div class="text-center">
                    <a target="_blank" href="../mentions.php">Mentions légales</a> - <a target="_blank" href="../charte.php">Charte d'utilisation</a> - <a target="_blank" href="../licences.php">Licences</a>
                </div>
            </footer>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Chosen -->
        <script src="../js/chosen.jquery.min.js"></script>

        <script>
            $('.chosen-select').chosen();
        </script>
    </body>
</html>