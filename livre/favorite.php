<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }

    // Si aucun livre_id n'est spécifier on renvoie vers la liste des critique
    if (!isset($_GET['livre_id'])) {
        header('Location: index.php');
        exit('Redirection... <a href="../auteur/index.php">Cliquez ici</a>');
    }

    $livre_id = $_GET['livre_id'];

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // Cette requête récupère le like de la critique par l'utilisateur
    $query = $db->prepare('SELECT
                            *
                            FROM utilisateur_livrespreferes
                            WHERE
                            utilisateur_id = ?
                            AND livre_id = ?');
    $query->execute(array(
        $user['id'],
        $livre_id
    ));

    // Si il n'y a pas de rang correspondant au livre et à l'utilisateur c'est qu'il ne l'a pas dans ses favoris
    if ($query->rowCount() == 0) {
        $query = $db->prepare('INSERT INTO utilisateur_livrespreferes
                                   -- VALUES(utilisateur_id, livre_id)
                                   VALUES(?, ?)');
        $query->execute(array($user['id'], $livre_id));

    } else {
        // Sinon il y'en a déjà une et donc on la supprime
        $query = $db->prepare('DELETE FROM utilisateur_livrespreferes
                                    WHERE
                                    utilisateur_id = ?
                                    AND livre_id = ?');
        $query->execute(array(
            $user['id'],
            $livre_id
        ));
    }

    header("Location: fiche.php?livre_id=$livre_id");
    exit('Redirection... <a href="fiche.php?livre_id='.$livre_id.'">Cliquez ici</a>');

