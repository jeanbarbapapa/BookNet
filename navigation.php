<?php
    $baseUrl = '/BookNet/';
?>

<nav class="aside-navbar">
    <div class="profile">
        <img src="<?php echo $baseUrl ?>image/avatar/<?php echo $user['avatar'] ?>" class="img-circle">
        <p>
            <b><?php echo $user['prenom'].' '.$user['nom'] ?> <a href="<?php echo $baseUrl ?>logout.php" class="logout"><i class="fa fa-times-circle-o"></i></a></b>
        </p>
    </div>
    <div class="items">
        <ul>
            <li <?php if($route == 'accueil'): ?>class="active"<?php endif ?>>
                <a href="<?php echo $baseUrl ?>index.php"><i class="fa fa-book"></i>Accueil</a>
            </li>
            <li <?php if($route == 'critique'): ?>class="active"<?php endif ?>>
                <a href="<?php echo $baseUrl ?>critique/index.php"><i class="fa fa-bookmark-o"></i>Critiques</a>
            </li>
            <li <?php if($route == 'livre'): ?>class="active"<?php endif ?>>
                <a href="<?php echo $baseUrl ?>livre/index.php"><i class="fa fa-book"></i>Livres</a>
            </li>
            <li <?php if($route == 'auteur'): ?>class="active"<?php endif ?>>
                <a href="<?php echo $baseUrl ?>auteur/index.php"><i class="fa fa-group"></i>Auteurs</a>
            </li>
            <li <?php if($route == 'profil'): ?>class="active"<?php endif ?>>
                <a href="<?php echo $baseUrl?>profil/index.php"><i class="fa fa-user"></i>Mon Profil</a>
            </li>
        </ul>
    </div>
</nav>