<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on renvoie le code 401 "Unauthorized"
        http_response_code(401);
        die('Veuillez vous connecter');
    }

    // On créer un tableau global pour stocker les potentielles erreurs
    $GLOBALS['erreurs'] = array();

    // On créer un dictionnaire global pour enregistrer les informations des différents champs du formulaire
    $GLOBALS['dictionnaire'] = array(
        'critique_id' => array('entity' => 'article', 'label' => 'nom auteur', 'type' => 'entity'),
        'commentaire_contenu' => array('maxlength' => 2000, 'label' => 'contenu commentaire', 'type' => 'string'),
    );

    // Si on détecte une valeur dans $_POST, c'est ce que le formulaire à été soumis
    if (isset($_POST['critique_id'])) {
        // On récupère les valeurs
        $critique_id = retrieve_input('critique_id');
        $commentaire_contenu = retrieve_input('commentaire_contenu');

        // Si il n'y a pas d'erreur on continue
        if (count($GLOBALS['erreurs']) == 0) {
            // Tentative connexion à la base de données
            try {
                $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            } catch (Exception $e) {
                // En cas d'erreur on quitte proprement en affichant un message controllé
                http_response_code(503);
                die('Impossible de se connecter à la base données');
            }

            $query = $db->prepare('INSERT INTO commentaire
                          -- VALUES(id, utilisateur_id, article_id, contenu, date)
                          VALUES(NULL, ?, ?, ?, NOW())');
            $query->execute(array(
                $user['id'],
                $critique_id,
                $commentaire_contenu
            ));

            // Si la query renvoit FALSE (= échec)
            if (!$query) {
                // On définit le code de statut sur 500 (= erreur du serveur)
                http_response_code(500);
            } else {
                // Sinon on défnit le code de statut sur 200 (= succes)
                http_response_code(200);
            }
            die('2');
        } else {
            // Sinon cela veut dire qu'il y'a une/plusieurs erreur(s), on renvoit un code d'erreur "Bad Request"
            http_response_code(500);
            die('Vérifier les informations que vous avez rentré');
        }
    }

    exit('erreur');



    function retrieve_input($input_name) {
        // On récupère la valeur depuis $_POST
        $value = $_POST[$input_name];
        $informations = $GLOBALS['dictionnaire'][$input_name];
        if ($informations['type'] == 'string') {
            // Si la valeur est un "?" on renvoit null
            if ($value == '?') {
                return null;
            }
            // On convertit value en string
            $value = strval($value);
            // Si la longueur de la chaine dépasse la longueur maximum on ajoute un erreur
            if (strlen($value) > $informations['maxlength']) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La longueur de $label n'est pas respectée";
            }
            // Si aucune valeur n'est spécifiée on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }

            // On sécurise la chaine de caractère en échappant tous les caractères spécifique à l'HTML
            $value = htmlspecialchars($value);
        } elseif ($informations['type'] == 'integer') {
            // Si la valeur est un "?" on renvoit null
            if ($value == '?') {
                return null;
            }
            // Si aucune valeur n'est précisé on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }
            // On convertit value en un entier
            $value = intval($value);
            // Si la valeur spécifiée dépasse les borne défini on ajoute une erreur
            if ($value < $informations['min'] || $value > $informations['max']) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
            } elseif ($informations['type'] == 'entity') {
                $value = intval($value);
                // Tentative connexion à la base de données
                try {
                    $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
                } catch (Exception $e) {
                    // En cas d'erreur on quitte proprement en affichant un message controllé
                    die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
                }
                // On prépare un requête pour vérifier si l'entité existe bel et bien
                $query = $db->prepare('SELECT * FROM '.$informations['entity'].' WHERE id = ?');
                $query->execute(array(
                    $value
                ));
                if ($query->rowCount() != 1) {
                    // Si elle n'existe pas on ajoute une erreur
                    $label = $informations['label'];
                    $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
                }
            }
        }

        // On retourne la valeur
        return $value;
    }