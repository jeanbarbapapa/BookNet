<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }

    // Si aucun critique_id n'est spécifier on renvoie vers la liste des critique
    if (!isset($_GET['critique_id'])) {
        header('Location: index.php');
        exit('Redirection... <a href="index.php">Cliquez ici</a>');
    }

    $critique_id = $_GET['critique_id'];

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // Cette requête récupère le like de la critique par l'utilisateur
    $query = $db->prepare('SELECT
                            id
                            -- On utilise les ` pour échapper LIKE qui est un mot clé SQL
                            FROM `like`
                            WHERE
                            utilisateur_id = ?
                            AND article_id = ?');
    $query->execute(array(
        $user['id'],
        $critique_id
    ));

    // Si il n'y a pas de rang correspondant à la critique et à l'utilisateur c'est qu'il ne l'a pas aimé
    if ($query->rowCount() == 0) {
        $query = $db->prepare('INSERT INTO `like`
                               -- VALUES(id, utilisateur_id, article_id, date)
                               VALUES(NULL, ?, ?, NOW())');
        $query->execute(array($user['id'], $critique_id));

    } else {
        // Sinon il y'en a déjà une et donc on la supprime
        $query = $db->prepare('DELETE FROM `like`
                                WHERE
                                utilisateur_id = ?
                                AND article_id = ?');
        $query->execute(array(
            $user['id'],
            $critique_id
        ));
    }

    header("Location: lire.php?critique_id=$critique_id");
    exit('Redirection... <a href="lire.php?article_id='.$critique_id.'">Cliquez ici</a>');

