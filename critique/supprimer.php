<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'critique';

    // Si aucun critique_id n'est spécifier on renvoie vers la liste des critique
    if (!isset($_GET['critique_id'])) {
        header('Location: utilisateur.php');
        exit('Redirection... <a href="utilisateur.php">Cliquez ici</a>');
    }

    // On récupère l'id de la critique si il est fournit sinon on met en valeur par défaut sur -1
    $critique_id = (isset($_GET['critique_id'])) ? $_GET['critique_id'] : -1;

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // Cette requête récupère les données de la critique ainsi que les données du livre associer plus celle de l'auteur du livre
    $query = $db->prepare('SELECT
                          article.id AS article_id,
                          article.utilisateur_id
                          FROM article
                          WHERE article.id = :critique_id');
    // On execute la requête en passant en argument l'id de la critique demandée
    $query->execute(array(':critique_id' => $critique_id));

    // Si aucune critique n'est trouvé, on renvoie vers la liste avec un code erreur
    if ($query->rowCount() != 1) {
        header('Location: utilisateur.php?code=404');
        exit('Redirection... <a href="utilisateur.php?code=404">Cliquez ici</a>');
    }

    // On récupère les données de la critique
    $critique = $query->fetch();

    // Si l'utilisateur n'est pas le propriétaire de la critique on le renvoie avec un code de manque de permission
    if ($critique['utilisateur_id'] != $user['id']) {
        header('Location: utilisateur.php?code=403');
        exit('Redirection... <a href="utilisateur.php?code=403">Cliquez ici</a>');
    }

    // On supprime d'abord tous les commentaires associés
    $query = $db->prepare('DELETE FROM commentaire WHERE commentaire.article_id = :critique_id');
    $query->execute(array(
        ':critique_id' => $critique_id,
    ));

    // On supprime ensuite tous les like associés
    $query = $db->prepare('DELETE FROM `like` WHERE `like`.article_id = :critique_id');
    $query->execute(array(
        ':critique_id' => $critique_id,
    ));

    // On supprime enfin la critique
    $query = $db->prepare('DELETE FROM article WHERE article.id = :critique_id');
    $query->execute(array(
        ':critique_id' => $critique_id,
    ));

    header('Location: utilisateur.php');
    exit('Redirection... <a href="utilisateur.php">Cliquez ici</a>');

?>