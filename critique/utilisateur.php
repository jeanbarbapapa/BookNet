<?php
    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'critique';

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    define("NOMBRE_CRITIQUE_PAR_PAGE", 16);

    // On détermines le nombre de page dont on a besoin
    $query = $db->query('SELECT COUNT(*) AS nombre_critique FROM article');
    $informations = $query->fetch();
    $informations['nombre_critique'] = intval($informations['nombre_critique']);

    $reste = $informations['nombre_critique'] % NOMBRE_CRITIQUE_PAR_PAGE;
    $quotient = ($informations['nombre_critique'] - $reste) / NOMBRE_CRITIQUE_PAR_PAGE;
    $nombrePage = $quotient;
    // Si quelques page sont en plus on ajoute une page de plus
    if ($reste > 0) {
        $nombrePage++;
    }

    // On récupère la page demandé, si aucune valeur n'est fournit ou est < 1 ou est > au nombre de page on donne la valeur 1
    $pageRequested = (isset($_GET['page']) && intval($_GET['page']) >= 1 && intval($_GET['page']) <= $nombrePage) ? intval($_GET['page']) : 1;

    // On détermine à partir de la page demandé l'offset et la limite pour la requête de sélectionne d'critique
    // L'offset correspond au rang à partir du quel on récupère
    // La limite correspond au nombre de rang que l'on récupère à partir de l'offset
    $offset = ($pageRequested - 1) * NOMBRE_CRITIQUE_PAR_PAGE;
    $limite = $offset + NOMBRE_CRITIQUE_PAR_PAGE;

    // On récupère les critiques de l'utilisateur connecté
    // préparation de la requête
    $query = $db->prepare("SELECT
                          article.id,
                          IF(article.titre = '', 'SANS TITRE', article.titre) AS critique_titre,
                          livre.titre AS livre_titre,
                          CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname,
                          CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS auteur_fullname,
                          IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), auteur.pseudo) AS auteur_shortname,
                          -- On utilise des sous-requete pour récupérer le nombre de likes et commentaires
                          -- On utilise les ` pour échapper LIKE qui est un mot clé SQL
                          (SELECT COUNT(*) FROM `like` WHERE `like`.article_id = article.id) AS count_like,
                          (SELECT COUNT(*) FROM commentaire WHERE commentaire.article_id = article.id) AS count_commentaire
                          FROM article
                          LEFT JOIN livre ON livre.id = article.livre_id
                          LEFT JOIN auteur ON auteur.id = livre.auteur_id
                          LEFT JOIN utilisateur ON utilisateur.id = article.utilisateur_id
                          WHERE article.utilisateur_id = ?
                          -- On ordonne par ordre chronologique inversé
                          ORDER BY article.date_edition DESC
                          -- On applique l'offset et la limite calculée plus haut
                          LIMIT $offset, $limite");
    $query->execute(array($user['id']));
    $critiques = $query->fetchAll();

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Mes critiques</title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/chosen.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">
            <div class="rowbox">
                <h2>Mes critiques</h2>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <a href="ajouter.php" class="btn btn-success btn-outline btn-block btn-sm">Rédiger une critique</a>
                    </div>
                    <div class="col-sm-4">
                        <a href="index.php" class="btn btn-success btn-outline btn-block btn-sm">Toutes les critiques</a>
                    </div>
                </div>
            </div>
            <div class="rowbox">
                <div>
                    <?php if (count($critiques) == 0): ?>
                        <p class="text-center">Vous n'avez encore écrit aucune critique... <a href="../critique/ajouter.php?livre_id=<?php echo $livre['livre_id'] ?>">Rédiger en  une</a> !</p>
                    <?php else: ?>

                        <p class="text-center">Retrouvez ici toutes vos critiques</p>
                        <div class="critiques-container">
                            <?php foreach($critiques as $i => $critique): ?>
                                <div class="row critique">
                                    <div class="col-sm-7">
                                        <h4 class="text-uppercase text-right">
                                            <a target="_self" href="../critique/lire.php?critique_id=<?php echo $critique['id'] ?>&from=<?php echo $livre['livre_id'] ?>"><?php echo $critique['critique_titre'] ?></a>
                                            <small class="text-danger"><i class="fa fa-heart fa-fw"></i> <?php echo $critique['count_like'] ?></small>
                                            <small class="text-success"><i class="fa fa-comment fa-fw"></i> <?php echo $critique['count_commentaire'] ?></small>
                                        </h4>
                                        <h5 class="text-uppercase text-right">
                                            <i><?php echo $critique['livre_titre'] ?></i><br>
                                            <small><?php echo $critique['auteur_shortname'] ?></small>
                                        </h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <p class="text-right" style="padding-top: 15px">
                                            <a href="editeur.php?critique_id=<?php echo $critique['id'] ?>" class="btn btn-primary btn-sm btn-outline"><i class="fa fa-pencil fa-fw"></i></a>
                                            <a href="supprimer.php?critique_id=<?php echo $critique['id'] ?>" class="btn btn-primary btn-sm btn-outline" onclick="return confirm('Êtes-vous sur de vouloir supprimer votre critique ?')"><i class="fa fa-trash fa-fw"></i></a>
                                        </p>
                                    </div>
                                    <div class="col-sm-12">
                                        <hr>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    <?php endif ?>
                </div>
                <ul class="pagination pagination-sm">
                    <?php if ($nombrePage > 1): ?>
                        <?php foreach (range(1, $nombrePage) as $pageId => $pageNumber): ?>
                            <li <?php if ($pageNumber == $pageRequested):?>class="active"<?php endif ?>><a href="?page=<?php echo $pageNumber ?>"><?php echo $pageNumber ?></a></li>
                        <?php endforeach ?>
                    <?php endif ?>
                </ul>
            </div>
            <footer>
                <div class="text-center">
                    <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
                </div>
            </footer>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Chosen -->
        <script src="../js/chosen.jquery.min.js"></script>

        <script>
            $('.chosen-select').chosen();
        </script>
    </body>
</html>