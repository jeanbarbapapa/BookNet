<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }

    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'accueil';

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    $utilisateur_id = $user['id'];
    $query = $db->query("SELECT *
                           FROM
                           (
                            -- Première sous requete pour récupérer les likes
                            SELECT
                            'like' AS notification_type,
                            'a aimé votre critique' AS notification_message,
                            article.titre AS notification_critique,
                            '' AS notification_book,
                            '' AS notification_auteur,
                            'Voir son profil' AS notification_link_text,
                            CONCAT('profil/index.php?utilisateur_id=', utilisateur.id) AS notification_link,
                            CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname,
                            `like`.date_action as notification_date
                            FROM `like`
                            LEFT JOIN article ON article.id = `like`.article_id
                            LEFT JOIN utilisateur ON utilisateur.id = `like`.utilisateur_id
                            -- On écarte la possibilité ou l'utilisateur a aimé lui même sa critique
                            WHERE article.utilisateur_id = $utilisateur_id AND `like`.utilisateur_id != $utilisateur_id

                              UNION ALL

                            -- Deuxième sous requete pour récupérer les nouveaux abonné
                            SELECT
                            'subscriber' AS notification_type,
                            'vous a suivi' AS notification_message,
                            '' AS notification_critique,
                            '' AS notification_book,
                            '' AS notification_auteur,
                            'Voir son profil' AS notification_link_text,
                            CONCAT('profil/index.php?utilisateur_id=', utilisateur.id) AS notification_link,
                            CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname,
                            abonnements.date_action as notification_date
                            FROM abonnements
                            LEFT JOIN utilisateur ON utilisateur.id = abonnements.utilisateur_abonne_id
                            WHERE abonnements.utilisateur_suivi_id = $utilisateur_id

                              UNION ALL

                            -- Troisième sous requête pour récupérer les commentaires sur les critique de l'utilisateur
                            SELECT
                              'comment' AS notification_type,
                              'a commenté votre critique sur' AS notification_message,
                              article.titre AS notification_critique,
                              livre.titre AS notification_book,
                              IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), auteur.pseudo) AS notification_auteur,
                              'Voir' AS notification_link_text,
                              CONCAT('critique/lire.php?critique_id=', article.id, '#C', commentaire.id) AS notification_link,
                              CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname,
                              commentaire.date as notification_date
                            FROM commentaire_b
                            LEFT JOIN article ON article.id = commentaire.article_id
                            LEFT JOIN livre ON livre.id = article.livre_id
                            LEFT JOIN auteur ON auteur.id = livre.auteur_id
                            LEFT JOIN utilisateur ON utilisateur.id = commentaire.utilisateur_id

                            -- On écarte la possibilité ou l'utilisateur a posté lui même le commentaire
                            WHERE article.utilisateur_id = $utilisateur_id AND commentaire.utilisateur_id != $utilisateur_id

                           ) notifications
                           ORDER BY notifications.notification_date DESC
                           LIMIT 0, 6");
    $notifications = $query->fetchAll();

    // On récupère les abonnements de l'utilisateur, c'est à dire les nouvelles critiques des gens auquel il est abonné
    $query = $db->query("SELECT
                            'article' AS notification_type,
                            'a publié une critique sur' AS notification_message,
                            article.titre AS notification_critique,
                            livre.titre AS notification_book,
                            IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), auteur.pseudo) AS notification_auteur,
                            'Lire' AS notification_link_text,
                            CONCAT('critique/lire.php?critique_id=', article.id) AS notification_link,
                            CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname,
                            article.date_creation as notification_date
                            FROM abonnements
                            RIGHT JOIN article ON article.utilisateur_id = abonnements.utilisateur_suivi_id
                            LEFT JOIN utilisateur ON utilisateur.id = article.utilisateur_id
                            LEFT JOIN livre ON livre.id = article.livre_id
                            LEFT JOIN auteur ON auteur.id = livre.auteur_id
                            WHERE abonnements.utilisateur_abonne_id = $utilisateur_id
                            ORDER BY article.date_creation DESC
                            LIMIT 0, 6");
    $abonnements = $query->fetchAll();





?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>BookNet</title>

    <!-- Bootstrap -->
    <link href="css/flatly.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body>
    <?php include ('navigation.php') ?>

    <div class="page-wrapper">
        <div class="rowbox">
            <h3 class="text-uppercase">Bonjour <?php echo $user['prenom'] ?></h3>
            <p>
                A partir d'ici vous pourrez suivre les nouvelles critiques de vos amis ainsi que les réactions de la
                communauté à vos propres critiques
            </p>
            <hr>
            <div class="container">
                <div class="col-sm-4">
                    <a class="btn btn-success btn-block" href="critique/ajouter.php">Nouvelle critique</a>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-success btn-block" href="livre/ajouter.php">Ajouter un livre</a>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-success btn-block" href="auteur/ajouter.php">Ajouter un auteur</a>
                </div>
            </div>

        </div>

        <div class="rowbox">
            <h3 class="text-uppercase border-bottom">Notifications</h3>
            <div class="row equaliseh" data-target=".subscription-alert">
                <?php if (count($notifications) == 0) :?>
                    <div>
                        <p class="text-center">Aucune notification</p>
                    </div>
                <?php endif ?>
                <?php foreach($notifications as $notification): ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="subscription-alert alert-<?php echo $notification['notification_type'] ?>">
                            <div>
                                <p>
                                    <b><?php echo $notification['utilisateur_fullname'] ?></b><br>
                                    <?php echo $notification['notification_message'] ?>
                                </p>
                                <?php if ($notification['notification_book'] != ''):?>
                                <p class="book">
                                    <span class="text-uppercase"><i><?php echo $notification['notification_book'] ?></i></span><br>
                                    <small>(<?php echo $notification['notification_auteur'] ?>)</small>
                                </p>
                                <?php endif ?>
                                <?php if ($notification['notification_link_text'] != ''):?>
                                <p class="text-center"><a href="<?php echo $notification['notification_link'] ?>" class="btn btn-sm"><?php echo $notification['notification_link_text'] ?></a></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>

        <div class="rowbox">
            <h3 class="text-uppercase border-bottom">Abonnements</h3>
            <div class="row equaliseh" data-target=".subscription-alert">
                <?php if (count($abonnements) == 0) :?>
                    <div>
                        <p class="text-center">Aucun abonnement à afficher</p>
                    </div>
                <?php endif ?>
                <?php foreach($abonnements as $abonnement): ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="subscription-alert alert-<?php echo $abonnement['notification_type'] ?>">
                            <div>
                                <p>
                                    <b><?php echo $abonnement['utilisateur_fullname'] ?></b><br>
                                    <?php echo $abonnement['notification_message'] ?>
                                </p>
                                <?php if ($abonnement['notification_book'] != ''):?>
                                    <p class="book">
                                        <span class="text-uppercase"><i><?php echo $abonnement['notification_book'] ?></i></span><br>
                                        <small>(<?php echo $abonnement['notification_auteur'] ?>)</small>
                                    </p>
                                <?php endif ?>
                                <?php if ($abonnement['notification_link_text'] != ''):?>
                                    <p class="text-center"><a href="<?php echo $abonnement['notification_link'] ?>" class="btn btn-sm"><?php echo $abonnement['notification_link_text'] ?></a></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>

        <footer>
            <div class="text-center">
                <a href="mentions.php">Mentions légales</a> - <a href="charte.php">Charte d'utilisation</a> - <a href="licences.php">Licences</a>
            </div>
        </footer>
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script>
    $('.equaliseh').each(function () {
        var height = 0;
        $($(this).data('target'), $(this)).each(function() {
            if ($(this).height() > height) {
                height = $(this).height()
            }
        });
        $($(this).data('target'), $(this)).height(height)
    })
</script>
</body>
</html>