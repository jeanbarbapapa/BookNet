<?php
    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'profil';

    // On créer un tableau global pour stocker les potentielles erreurs
    $GLOBALS['erreurs'] = array();

    // On récupère l'id de l'utilisateur si il est fournit sinon on met en valeur par défaut l'id de l'utilisateur actuel
    $utilisateur_id = (isset($_GET['utilisateur_id'])) ? $_GET['utilisateur_id'] : $user['id'];

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // préparation de la requête pour récupérer l'auteur
    $smallTag = ['<small>', '</small>'];
    $utilisateur_abonne_id = $user['id'];
    $query = $db->prepare("SELECT
                              utilisateur.id,
                              utilisateur.pseudo,
                              CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname,
                              -- On utilise des sous-requete pour récupérer le fait que l'utilisateur soit abonné
                              EXISTS (SELECT * FROM abonnements WHERE abonnements.utilisateur_suivi_id = utilisateur.id AND abonnements.utilisateur_abonne_id = $utilisateur_abonne_id) AS utilisateur_abonne
                              FROM utilisateur
                              WHERE utilisateur.id = ?");
    // On execute la requête en passant en argument l'id de l'auteur voulu
    $query->execute(array($utilisateur_id));
    if ($query->rowCount() == 1) {
        // Si il y'a bien un auteur on stocke ses infos dans une variable
        $utilisateur = $query->fetch();
        // Si l'auteur existe, on va récupérer les critiques qui lui sont associés
        $query = $db->prepare('SELECT
                              article.id,
                              IF(article.titre = "", "SANS TITRE", article.titre) AS critique_titre,
                              livre.titre AS livre_titre,
                              CONCAT(utilisateur.prenom, " ", utilisateur.nom) AS utilisateur_fullname,
                              IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, \' \', auteur.nom), auteur.pseudo) AS auteur_shortname,
                              -- On utilise des sous-requete pour récupérer le nombre de likes et commentaires
                              -- On utilise les ` pour échapper LIKE qui est un mot clé SQL
                              (SELECT COUNT(*) FROM `like` WHERE `like`.article_id = article.id) AS count_like,
                              (SELECT COUNT(*) FROM commentaire WHERE commentaire.article_id = article.id) AS count_commentaire
                              FROM article
                              LEFT JOIN livre ON livre.id = article.livre_id
                              LEFT JOIN auteur ON auteur.id = livre.auteur_id
                              LEFT JOIN utilisateur ON utilisateur.id = article.utilisateur_id
                              WHERE
                              article.utilisateur_id = ?
                              -- On récupère par ordre chronologique inversé de la date d\'édition
                              ORDER BY article.date_edition DESC');
        // On execute la requête en passant en argument l'id du livre demandé
        $query->execute(array($utilisateur['id']));
        // On stockes les critiques sous l'index "critiques" de la variable dictionnaire "livre"
        $utilisateur['critiques'] = $query->fetchAll();

        // On récupère les livres que l'utilisateur a ajouté en favoris
        // Préparation de la requête
        $query = $db->prepare("SELECT
                            livre.id,
                            livre.titre,
                            -- Si auteur.pseudo est NULL on récupère une string concaténant prenom et nom
                            -- Sinon on recupère une string concatéant le pseudo avec le prenom et le nom en petit
                            IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), auteur.pseudo) AS auteur_shortname,
                            livre.date
                            FROM livre
                            LEFT JOIN auteur ON auteur.id = livre.auteur_id
                            INNER JOIN utilisateur_livrespreferes 
                              ON utilisateur_livrespreferes.livre_id = livre.id
                            WHERE utilisateur_livrespreferes.utilisateur_id = ?
                            ORDER BY livre.titre");
        $query->execute(array($utilisateur_id));
        $utilisateur['livres_preferes'] = $query->fetchAll();

        // On récupère les auteurs que l'utilisateur a ajouté en favoris
        // Préparation de la requête
        $query = $db->prepare("SELECT
                            auteur.id,
                            -- Si la date de naissance est NULL on recupère un '?'
                            IFNULL(auteur.naissance, \"?\") AS naissance,
                            -- Si la date de decès est NULL on recupère un '?'
                            IFNULL(auteur.mort, \"?\") AS mort,
                            -- Si auteur.pseudo est NULL on récupère une string concaténant prenom et nom
                            -- Sinon on recupère une string concatéant le pseudo avec le prenom et le nom en petit
                            IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), auteur.pseudo) AS auteur_shortname
                            FROM auteur
                            INNER JOIN utilisateur_auteurspreferes
                               ON utilisateur_auteurspreferes.auteur_id = auteur.id
                            WHERE utilisateur_auteurspreferes.utilisateur_id = ?
                            ORDER BY auteur.nom");
        $query->execute(array($utilisateur_id));
        $utilisateur['auteurs_preferes'] = $query->fetchAll();
        
    } else {
        // Sinon on place la valeur de la variable sur null, c'est à dire rien
        $utilisateur = null;
        // On ajoute une erreur pour l'afficher
        $GLOBALS['erreurs'][] = "L'utilisateur demandé n'existe pas...";
    }

    # On vérfie si on a transmis un livre via "from"
    if (isset($_GET['from'])) {
        $critique_id = $_GET['from'];

        // préparation de la requête pour récupérer le critique
        $query = $db->prepare('SELECT
                               *
                               FROM article
                               WHERE
                               id = ?');
        $query->execute(array($critique_id));
        if ($query->rowCount() == 1) {
            // Si il y'a bien un critique on stocke ses infos dans une variable
            $critique_from = $query->fetch();
        } else {
            // Sinon on place la valeur de la variable sur null, c'est à dire rien
            $critique_from = null;
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Profil: <?php echo $utilisateur['utilisateur_fullname'] ?></title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/chosen.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">

            <?php if (count($GLOBALS['erreurs']) > 0): ?>
                <div class="rowbox">
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <? foreach($GLOBALS['erreurs'] as $erreur): ?>
                                    <li><?php echo $erreur ?></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                </div>
            <?php endif ?>


            <div class="rowbox">
                <?php if (isset($utilisateur) && $utilisateur): ?>

                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-1">
                            <p style="padding-top: 10px" class="text-center"><i class="fa fa-user fa-5x"></i></p>
                        </div>
                        <div class="col-sm-9" style="margin-top: 10px">
                            <h2 class="no-margin">
                                <em><strong><?php echo $utilisateur['utilisateur_fullname'] ?></strong></em>
                                <?php if($utilisateur['utilisateur_abonne']): ?><span class="text-grey"><i class="fa fa-rss-square fa-fw"></i></span><?php endif ?>
                            </h2>
                            <h2 class="no-margin"><small class="small-indent"><?php echo $utilisateur['pseudo'] ?></small></h2>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-6 col-md-offset-2">
                            <hr style="margin-bottom: 10px" class="no-margin">
                            <div class="row">
                                <?php if ($user['id'] == $utilisateur['id']): ?>
                                    <div class="col-sm-12">
                                        <a href="../critique/utilisateur.php" class="btn btn-success btn-outline btn-block btn-sm">
                                            <i class="fa fa-list fa-fw"></i> Editer mes critiques
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <div class="col-sm-6">
                                        <a href="subscribe.php?utilisateur_id=<?php echo $utilisateur['id'] ?>" class="btn btn-success btn-outline btn-block btn-sm">
                                            <?php if($utilisateur['utilisateur_abonne'] > 0): ?>
                                                <i class="fa fa-user-times fa-fw"></i> Se désabonner
                                            <?php else: ?>
                                                <i class="fa fa-user-plus fa-fw"></i> S'abonner
                                            <?php endif ?>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php if (isset($critique_from) && $critique_from): ?>
                                            <a target="_self" class="btn btn-primary btn-outline btn-block btn-sm" href="../critique/lire.php?critique_id=<?php echo $critique_from['id'] ?>"><i class="fa fa-bookmark fa-fw"></i> <i><?php echo $critique_from['titre'] ?></i></a>
                                        <?php else: ?>
                                            <a target="_self" class="btn btn-primary btn-outline btn-block btn-sm" href="index.php"><i class="fa fa-user fa-fw"></i> Mon profil</a>
                                        <?php endif ?>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="rowbox">
                    <div class="container-fluid">

                        <h3 class="text-uppercase" style="margin-bottom: 25px">Critiques</h3>

                        <div>
                            <ul class="list-inline list-item list-critique">
                                <?php if(count($utilisateur['critiques']) == 0): ?>
                                    <p class="text-center"><?php echo $utilisateur['utilisateur_fullname'] ?> n'a pas encore écrit de critique</p>
                                <?php endif ?>
                                <?php foreach($utilisateur['critiques'] as $critique): ?>
                                    <li class="text-center">
                                        <a href="../critique/lire.php?critique_id=<?php echo $critique['id'] ?>&from=<?php echo $utilisateur['id'] ?>">
                                            <div class="head">
                                                <p class="titre"><?php echo $critique['critique_titre'] ?></p>
                                                <p><i class="fa fa-heart-o fa-fw"></i> <?php echo $critique['count_like'] ?><!-- Deux espace insécable pour aérer la vue -->&nbsp;&nbsp;<i class="fa fa-comment-o fa-fw"></i> <?php echo $critique['count_commentaire'] ?></p>
                                            </div>

                                            <div class="infos">
                                                <p><i><?php echo $critique['livre_titre'] ?></i></p>
                                                <p><small>de <?php echo $critique['auteur_shortname'] ?></small></p>

                                            </div>
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="rowbox">
                    <div class="container-fluid">

                        <h3 class="text-uppercase" style="margin-bottom: 25px">Livres préférés</h3>

                        <div>
                            <ul class="list-inline list-item list-livre">
                                <?php if(count($utilisateur['livres_preferes']) == 0): ?>
                                    <p class="text-center"><?php echo $utilisateur['utilisateur_fullname'] ?> n'a pas de livre préféré</p>
                                <?php endif ?>
                                <?php foreach($utilisateur['livres_preferes'] as $livre): ?>
                                    <li class="text-center">
                                        <a href="../livre/fiche.php?livre_id=<?php echo $livre['id'] ?>">
                                            <div class="titre">
                                                <p><i><?php echo $livre['titre'] ?></i></p>
                                            </div>

                                            <div class="infos">
                                                <p><small><?php echo $livre['auteur_shortname'] ?></small></p>
                                                <p><small>(<?php echo $livre['date'] ?>)</small></p>
                                            </div>
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="rowbox">
                    <div class="container-fluid">

                        <h3 class="text-uppercase" style="margin-bottom: 25px">Auteurs préférés</h3>

                        <div>
                            <ul class="list-inline list-item list-auteur">
                                <?php if(count($utilisateur['livres_preferes']) == 0): ?>
                                    <p class="text-center"><?php echo $utilisateur['utilisateur_fullname'] ?> n'a pas d'auteur préféré</p>
                                <?php endif ?>
                                <?php foreach($utilisateur['auteurs_preferes'] as $auteur): ?>
                                    <li class="text-center">
                                        <a href="../auteur/fiche.php?auteur_id=<?php echo $auteur['id'] ?>">
                                            <i class="fa fa-user fa-lg"></i><br>
                                            <?php echo $auteur['auteur_shortname'] ?><br>
                                            <small><?php echo $auteur['naissance'].' - '.$auteur['mort'] ?></small>
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endif ?>

                <footer>
                    <div class="text-center">
                        <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
                    </div>
                </footer>
            </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Chosen -->
        <script src="../js/chosen.jquery.min.js"></script>

        <script>
            $('.chosen-select').chosen();
        </script>
    </body>
</html>