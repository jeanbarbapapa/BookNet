
// Credit: Chris West
String.prototype.replaceAll = function(target, replacement) {
    return this.split(target).join(replacement);
};

$(function() {
    // Création des variables représentant les éléments HTML
    var editeur = $('.editeur');
    var toolbar = $('.editeur-toolbar');
    var footbar = $('.editeur-footbar');
    var components = $('.editeur-components');
    var form = $('.editeur-form');

    // Enregistrement d'une action lors du "click" sur un bouton de la toolbar
    toolbar.on('click', '.btn', function(e) {
        // On récupère la valeur de l'attribut data-type du bouton
        var type = $(this).data('type');

        // On ajoute à l'éditeur l'élément lui correspondant (titre, paragraphe, citation)
        editeur.append($('.'+type, components).html());
    });

    // Enregistrement d'une action lors du "click" sur la croix d'un élement
    editeur.on('click', '.toolbar .fa-times', function(e){
        // On supprime l'élement
        var cellContainer = $(this).parents('.cell-container');
        cellContainer.remove();
    });

    // Enregistrement d'une action lors du "click" sur la flèche vers le haut d'un élement
    editeur.on('click', '.toolbar .fa-arrow-up', function(e) {
        // On récupère la cellule sélectionnée
        var cellContainer = $(this).parents('.cell-container');
        // On calcule son index
        var index = $('.cell-container', editeur).index(cellContainer);
        // On récupère la cellule au dessous
        var cellContainerBefore = $('.cell-container', editeur).eq(index - 1);
        // On enlève la cellule
        cellContainer.remove();
        // Si il y'a bien un élement au dessus et que son index est supérieur ou égal à 0 on place la cellule avant
        // sinon on le place au début de l'éditeur
        if (cellContainerBefore.length != 0 && index-1 >= 0) {
            cellContainerBefore.before(cellContainer);
        } else {
            editeur.prepend(cellContainer);
        }
    });

    // Enregistrement d'une action lors du "click" sur la flèche vers le bas d'un élement
    editeur.on('click', '.toolbar .fa-arrow-down', function(e) {
        // On récupère la cellule sélectionnée
        var cellContainer = $(this).parents('.cell-container');
        // On calcule son index
        var index = $('.cell-container', editeur).index(cellContainer);
        // On récupère la cellule en dessous
        var cellContainerAfter = $('.cell-container', editeur).eq(index + 1);
        // On enlève la cellule
        cellContainer.remove();

        // Si il y'a bien un élement en dessous on place la cellule après sinon on le place à la fin de l'éditeur
        if (cellContainerAfter.length != 0) {
            cellContainerAfter.after(cellContainer);
        } else {
            editeur.append(cellContainer);
        }
    });

    // Enregistrement d'une action lors du "click" sur le bouton "Sauvegarder"
    footbar.on('click', '.btn-success', function(e) {
        var elements = [];
        // On récupère les informations de chaque éléments
        $('.cell-container', editeur).each(function() {
            // On récupère le type de l'élément
            var type = $(this).data('type');

            var el = {'type':type};
            el.contenu = clear($('.contenu', $(this)).html());

            if (type == 'quote') {
                el.source = clear($('.source', $(this)).html());
            }

            elements.push(el);
        });

        // On place le tableaux des éléments sous format json dans l'input prévue à cette effet
        $('input[name="critique_contenu"]', form).val(JSON.stringify(elements));

        // On soumet le formulaire
        form.submit()
    });

    // On nettoie la chaine de toutes ses "impuretés HTML" crée par le navigateur, pour récupérer un texte propre
    function clear(string) {
        string = String(string);
        string = string.replaceAll('<br>', '');
        string = string.replaceAll('</div>', '');
        string = string.replaceAll('<div>', '\n');
        string = string.replaceAll('&nbsp;', ' ');

        return string;
    }
});