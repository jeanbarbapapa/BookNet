<?php

    session_start();

    // Si l'utilisateur est déjà connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        header('Location: index.php');
        exit('Redirection... <a href="index.php">Cliquez ici</a>');
    }

    // On créer un tableau global pour stocker les potentielles erreurs
    $GLOBALS['erreurs'] = array();

    // On place des valeurs par défaut
    $email = '';

    if (isset($_GET['email'])) {
        $email = urldecode($_GET['email']);
    }

    // On vérfie si le formulaire est soumis
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];

        // On sécurise les deux chaines
        $email = htmlspecialchars($email);
        $password = htmlspecialchars($password);

        // Tentative connexion à la base de données
        try {
            $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        } catch (Exception $e) {
            // En cas d'erreur on quitte proprement en affichant un message controllé
 die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
        }

        // On si l'email correspond à un utilisateur et on ses informations
        // préparation de la requête
        $query = $db->prepare('SELECT * FROM utilisateur WHERE email = ?');
        // Exécution en plaçant les valeurs
        $query->execute(array(
            $email
        ));
        // Si il y'a bien un utilisateur qui correspond
        if ($query->rowCount() == 1) {

            $user = $query->fetch();
            // On vérifie si le mot de passe fournit haché correspond bien au mot de passe stocké dans la base de données
            if (password_verify($password, $user['password'])) {
                // Si c'est bon on initialise les informations de sessions
                $_SESSION['user'] = array(
                    'id' => $user['id'],
                    'prenom' => $user['prenom'],
                    'nom' => $user['nom'],
                    'pseudo' => $user['pseudo'],
                    'email' => $user['email'],
                    'avatar' => $user['avatar']
                );
                $_SESSION['logged_in'] = true;

                // On le redirige vers l'accueil du site
                header('Location: index.php');
                exit('Redirection... <a href="index.php">Cliquez ici</a>');
            } else {
                // Sinon on crée une erreur
                $GLOBALS['erreurs'][] = "Les informations de connexion ne sont pas valides...";
            }
        } else {
            // Sinon on crée une erreur
            $GLOBALS['erreurs'][] = "Les informations de connexion ne sont pas valides...";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Connexion</title>

    <!-- Bootstrap -->
    <link href="css/flatly.min.css" rel="stylesheet">

    <style>
        #connexion {
            margin-top: 300px;
        }
        footer {
            position: absolute;
            bottom: 0;
            left: 0;
            margin-top: 15px;
            padding: 15px;
            background-color: white;
            width: 100%;
        }
    </style>
</head>
<body>

    <div id="connexion" class="col-md-4 col-md-offset-4">
        <?php if (isset($_GET['from']) && $_GET['from'] == 'inscription'): ?>
            <div class="alert alert-success" role="alert">
                <p>
                    <b>Votre compte a été créé avec succès !</b><br>
                    Il ne vous reste plus qu'à vous connecter
                </p>
            </div>
        <?php endif ?>
        <?php if (count($GLOBALS['erreurs']) > 0): ?>
            <div class="alert alert-danger" role="alert">
                <ul>
                    <? foreach($GLOBALS['erreurs'] as $erreur): ?>
                        <li><?php echo $erreur ?></li>
                    <?php endforeach ?>
                </ul>
            </div>
        <?php endif ?>
        <form method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Adresse email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $email ?>">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Mot de passe</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe">
            </div>
<!--            <div class="form-group">-->
<!--                <div class="checkbox">-->
<!--                    <label>-->
<!--                        <input type="checkbox"> se souvenir de moi-->
<!--                    </label>-->
<!--                </div>-->
<!--            </div>-->

            <button type="submit" class="btn btn-success btn-block">Connexion</button>
            <p class="text-center">Pas de compte ? <a href="inscription.php">Cliquez ici</a> pour vous inscrire</p>
        </form>
    </div>
    <footer>
        <div class="text-center">
            <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
        </div>
    </footer>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>