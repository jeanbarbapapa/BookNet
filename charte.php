<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on définit utilisateur à null, c'est à dire rien
        $user = null;
    }

    // On définit la route actuelle pour l'affichage dans la navigation
    $route = '';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Charte d'utilisation</title>

    <!-- Bootstrap -->
    <link href="css/flatly.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body class="<?php if ($user === null): ?>bg-grey<?php endif?>">

    <?php
    // Si l'utilisateur n'est pas null c'est qu'il est connecté, on affiche donc la barre de navigation
        if ($user !== null) {
            include ('navigation.php');
        }
    ?>

    <div class="<?php if ($user !== null): ?>page-wrapper<?php else: ?>container<?php endif?>">
        <div class="rowbox">
            <h3 class="text-uppercase <?php if ($user === null): ?>text-center<?php endif?>">Charte d'utilisation</h3>
            <div class="charte-container">
                <h3 class="text-uppercase border-bottom">Préambule</h3>
                <p>
                    BookNet met en œuvre un système d'information sur différents livres, ce site permet à son utilisateur
                    de donner son avis sur l’œuvre qu'il souhaite ou commenter la critique d'une autre personne.
                </p>
                <p>
                    La charte fait partie intégrante des conditions d’utilisation. L’objectif de la Charte est d’instaurer
                    la confiance dans l’utilisation du site et de préserver l’intégrité et le bon fonctionnement de ce système,
                    dans le respect des droits et des libertés de chacun. Elle s'applique donc à l'ensemble des utilisateurs.
                </p>
            </div>
        </div>

        <div class="rowbox">
            <div class="charte-container">

                <h3 class="text-uppercase border-bottom">1. Confidentialité des paramètres d'accès</h3>
                <p>
                    L'accès à certains éléments du système (comme la publication d'un commentaire, les services interactifs, ...)
                    est protégé par des paramètres de connexion (identifiants, mots de passe).
                </p>
                <p>
                    Ces paramètres sont personnels à l'utilisateur et doivent être gardés confidentiels. Ils permettent en
                    particulier de contrôler l'activité des utilisateurs. Ils doivent être saisis par l'utilisateur à chaque
                    accès ou ils peut être conservés en mémoire dans le site.
                </p>
                <p>
                    L’utilisateur doit choisir son mot de passe avec des caractères alphanumériques et/ou chiffres. Le mot
                    de passe doit être minimum de 8 caractères.<br>
                    Les mots de passe sont personnels et ne doivent pas être communiqués à une autre personne
                </p>

                <h3 class="text-uppercase border-bottom">2. Protection des ressources sous la responsabilité de l'utilisateur</h3>
                <p>
                    L'entreprise met en œ uvre les moyens techniques appropriés pour assurer la sécurité du site. À ce titre,
                    il lui appartient de limiter les accès aux ressources sensibles et d'acquérir les droits de propriété
                    intellectuelle ou d'obtenir les autorisations nécessaires à l'utilisation des ressources mises à disposition
                    des utilisateurs.Il veille à l'application des règles de la présente charte.
                </p>

                <h3 class="text-uppercase border-bottom">3. Droit d'auteur</h3>
                <p>
                    L’utilisateur à l’accès à ce site s’engageant à ne pas utiliser cet accès à des fins de reproduction, de
                    représentation, de mise à disposition ou de communication au public d’œuvres ou d’objets protégés
                    par un droit d’auteur ou par un droit voisin, tels que des textes, images, photographies sans
                    autorisation.
                </p>

                <h3 class="text-uppercase border-bottom">4. Respect de la législation</h3>
                <p>
                    L’utilisateur s’engage à respecter la législation en vigueur, et notamment à n’utiliser le Service que :
                    <ul>
                        <li>Dans le respect des lois relatives à la propriété littéraire et artistique</li>
                        <li>Dans le respect des lois relatives à l’informatique, aux fichiers et aux libertés</li>
                        <li>Dans le respect des règles relatives à la protection de la vie privée et notamment du droit à l’image d’autrui</li>
                        <li>En s’assurant de ne pas envoyer de messages à caractère raciste, pornographique, injurieux, diffamatoire, etc...</li>
                    </ul>
                    Et de manière générale à ne pas diffuser d’informations présentant le caractère d’une infraction et ne pas
                    porter atteinte à l'intégrité d'une personne ou à sa sensibilité, notamment par l'intermédiaire de commentaires ou critiques provocants.
                </p>

                <h3 class="text-uppercase border-bottom">5. Sanctions</h3>
                <p>
                    Le manquement aux règles et mesures de sécurité de la présente charte est susceptible d'engager la
                    responsabilité de l'utilisateur et d'entraîner à son encontre une suspension de son compte, lui
                    empéchant donc d'utiliser le site.
                </p>

            </div>
        </div>
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script>
    $('.equaliseh').each(function () {
        var height = 0;
        $($(this).data('target'), $(this)).each(function() {
            if ($(this).height() > height) {
                height = $(this).height()
            }
        });
        $($(this).data('target'), $(this)).height(height)
    })
</script>
</body>
</html>